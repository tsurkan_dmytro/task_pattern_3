package epam.task.pattern3.deliveryvar;

import epam.task.pattern3.BouquetDecorator;

public class SlowDelivery extends BouquetDecorator {
    private final double COST = 8;
    private final String DELIVERY = "   :>Slow";

    public SlowDelivery() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setDeliveryType(DELIVERY);
    }
}
