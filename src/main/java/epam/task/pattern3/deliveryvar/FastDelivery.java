package epam.task.pattern3.deliveryvar;

import epam.task.pattern3.BouquetDecorator;

public class FastDelivery extends BouquetDecorator {
    private final double COST = 22;
    private final String DELIVERY = "   :>Fast";

    public FastDelivery() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setDeliveryType(DELIVERY);
    }
}
