package epam.task.pattern3.deliveryvar;

import epam.task.pattern3.BouquetDecorator;

public class SuperDelivery extends BouquetDecorator {
    private final double COST = -5;
    private final String DELIVERY = "  :>Super DELIVERY";

    public SuperDelivery() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setDeliveryType(DELIVERY);
    }
}
