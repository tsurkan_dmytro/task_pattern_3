package epam.task.pattern3.deliveryvar;

import epam.task.pattern3.BouquetDecorator;

public class FreeDelivery extends BouquetDecorator {
    private final double COST = 0;
    private final String DELIVERY = "   :>Free";

    public FreeDelivery() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setDeliveryType(DELIVERY);
    }
}