package epam.task.pattern3.storefcatory;

import epam.task.pattern3.Bouquet;
import epam.task.pattern3.deliveryvar.SlowDelivery;
import epam.task.pattern3.discountvar.SuperDiscont;
import epam.task.pattern3.events.BirthDayDecorator;
import epam.task.pattern3.events.SimpleBouquet;
import epam.task.pattern3.packsvar.CreativePack;

public class ConcreteBirthDayFreeType extends AbstractDelivery {

    @Override
    public void create() {
        Bouquet  simple = new SimpleBouquet();
        BirthDayDecorator birthDay = new BirthDayDecorator();
        SuperDiscont discont = new SuperDiscont();
        SlowDelivery delivery = new SlowDelivery();
        CreativePack pack = new CreativePack();

        birthDay.setBouquet(new SimpleBouquet());
        discont.setBouquet(birthDay);
        delivery.setBouquet(discont);
        pack.setBouquet(delivery);

        pack.create();

        System.out.println("BirthDay bouquet " + pack.getName());
        System.out.println("Price " + pack.getPrice());
        System.out.println("Type Delivery " + pack.getDeliveryType());
        System.out.println("Discount " + discont.getDiscount());
        System.out.println("Flowers " + pack.getFlowersCount());
        System.out.println("Pack type " + pack.getPackType());
        System.out.println(simple);
    }
}