package epam.task.pattern3.storefcatory;

import epam.task.pattern3.Bouquet;
import epam.task.pattern3.BouquetDecorator;
import epam.task.pattern3.events.BirthDayDecorator;
import epam.task.pattern3.events.SimpleBouquet;

public class ConcreteBirthDay extends AbstractDelivery {

    @Override
    public void create() {
        Bouquet  simple = new SimpleBouquet();
        BouquetDecorator birthDay = new BirthDayDecorator();
        birthDay.setBouquet(simple);
        birthDay.create();
        System.out.println("BirthDay bouquet " + birthDay.getName());
        System.out.println("Price " + birthDay.getPrice());
    }
}