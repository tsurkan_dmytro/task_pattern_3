package epam.task.pattern3.storefcatory;

import epam.task.pattern3.Bouquet;
import epam.task.pattern3.events.SimpleBouquet;

public class ConcreteBouquetSimple extends AbstractDelivery {

    @Override
    public void create() {
        Bouquet  simple = new SimpleBouquet();
        simple.create();
        System.out.println("Price " + simple.getPrice());

    }
}