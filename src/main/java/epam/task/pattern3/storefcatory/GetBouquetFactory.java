package epam.task.pattern3.storefcatory;

public class GetBouquetFactory{

    //use getPlan method to get object of type bouquetType
    public AbstractDelivery getDelivery(String bouquetType){
        if(bouquetType == null){
            return null;
        }
        if(bouquetType.equalsIgnoreCase("1")) {
            return new ConcreteBouquetSimple();
        }
        else if(bouquetType.equalsIgnoreCase("2")){
            return new ConcreteValentainsDay();
        }
        else if(bouquetType.equalsIgnoreCase("3")) {
            return new ConcreteBirthDayFreeType();
        }
        else if(bouquetType.equalsIgnoreCase("4")) {
            return new ConcreteBirthDay();
        }
        return null;
    }
}
