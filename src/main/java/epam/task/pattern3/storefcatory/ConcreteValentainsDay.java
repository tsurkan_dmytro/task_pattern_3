package epam.task.pattern3.storefcatory;

import epam.task.pattern3.Bouquet;
import epam.task.pattern3.BouquetDecorator;
import epam.task.pattern3.deliveryvar.FastDelivery;
import epam.task.pattern3.events.SimpleBouquet;
import epam.task.pattern3.events.ValentainsDayDecorator;

public class ConcreteValentainsDay extends AbstractDelivery {

    @Override
    public void create() {
        Bouquet  simple = new SimpleBouquet();
        BouquetDecorator bouquetDecorator = new ValentainsDayDecorator();
        bouquetDecorator.setBouquet(simple);
        BouquetDecorator delivery = new FastDelivery();
        delivery.setBouquet(bouquetDecorator);
        delivery.create();
        System.out.println("Valentains Day bouquet " + delivery.getName());
        System.out.println("Price " + delivery.getPrice());
        System.out.println("Deliv Type " + delivery.getDeliveryType());
    }
}