package epam.task.pattern3;

public interface Bouquet {
    void create();
    double getPrice();
    String getName();
    int getFlowersCount();
}