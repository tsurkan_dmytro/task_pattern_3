package epam.task.pattern3.packsvar;

import epam.task.pattern3.BouquetDecorator;

public class GlassPack extends BouquetDecorator {
    private final double COST = 12;
    private final String PACKTYPE = "   :Glass";

    public GlassPack() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setPackType(PACKTYPE);
    }
}
