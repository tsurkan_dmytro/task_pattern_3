package epam.task.pattern3.packsvar;

import epam.task.pattern3.BouquetDecorator;

public class CreativePack extends BouquetDecorator {
    private final double COST = 14;
    private final String PACKTYPE = "   :Creative";

    public CreativePack() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setPackType(PACKTYPE);
    }
}
