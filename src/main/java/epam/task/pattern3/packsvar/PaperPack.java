package epam.task.pattern3.packsvar;

import epam.task.pattern3.BouquetDecorator;

public class PaperPack extends BouquetDecorator {
    private final double COST = 5;
    private final String PACKTYPE = "   :Paper";

    public PaperPack() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setPackType(PACKTYPE);
    }
}