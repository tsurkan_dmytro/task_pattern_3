package epam.task.pattern3.events;

import epam.task.pattern3.BouquetDecorator;

public class BirthDayDecorator extends BouquetDecorator {
    private final double COST = 18;
    private final String TO_EVENT = "  >> BirthDay";
    private final int FLOWERS = 2;

    public BirthDayDecorator() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setToName(TO_EVENT);
        setAdditionalFlowers(FLOWERS);
    }
}