package epam.task.pattern3.events;

import epam.task.pattern3.Bouquet;

public class SimpleBouquet implements Bouquet {

    final private double PRICE = 10;
    final private String NAME_BOUQUET = "Simple Bouquet";
    private String packType = "Standart";
    private int flowersCount = 3;
    private String deliveryType = "No";

    @Override
    public void create() {
        System.out.println("Simple bouquet created");
    }

    public double getPrice() {
        return PRICE;
    }

    @Override
    public String getName() {
        return NAME_BOUQUET;
    }

    public void setFlowersCount(int flowersCount) {
        this.flowersCount = flowersCount;
    }

    public int getFlowersCount() {
        return flowersCount;
    }

    public String getPackType() {
        return packType;
    }

    public void setPackType(String packType) {
        packType = packType;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    @Override
    public String toString() {
        return "SimpleBouquet{" +
                "PRICE= " + PRICE +
                ", NAME_BOUQUET= " + NAME_BOUQUET +
                ", flowersCount= " + flowersCount +
                ", packType= " + packType +
                ", deliveryType= " + deliveryType +
                '}';
    }
}