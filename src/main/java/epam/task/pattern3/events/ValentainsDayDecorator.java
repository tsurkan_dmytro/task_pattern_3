package epam.task.pattern3.events;

import epam.task.pattern3.BouquetDecorator;

public class ValentainsDayDecorator extends BouquetDecorator {
    private final double COST = 12;
    private final String TO_EVENT = "  >> Valentains Day";
    private final int FLOWERS = 4;

    public ValentainsDayDecorator() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setToName(TO_EVENT);
        setAdditionalFlowers(FLOWERS);
    }
}