package epam.task.pattern3.events;

import epam.task.pattern3.BouquetDecorator;

public class MatherDayDecorator extends BouquetDecorator {
    private final double COST = 5;
    private final String TO_EVENT = "  >> Mather Day";
    private final int FLOWERS = 8;

    public MatherDayDecorator() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setToName(TO_EVENT);
        setAdditionalFlowers(FLOWERS);
    }
}