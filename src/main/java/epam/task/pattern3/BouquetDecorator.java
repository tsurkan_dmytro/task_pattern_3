package epam.task.pattern3;

import java.util.Optional;

public abstract class BouquetDecorator implements Bouquet {

    private Bouquet core;

    private Optional<Bouquet> bouquet;
    private double additionalPrice;
    private String toName = "";
    private String deliveryType = "";
    private String discount = "";
    private String packType = "";
    private int flowersCount = 0;


    public void setBouquet(Bouquet outBouquet) {
        bouquet = Optional.ofNullable(outBouquet);
    }

    protected void setAdditionalPrice(double additionalPrice)
    {
        this.additionalPrice = additionalPrice;
    }

    protected void setAdditionalFlowers(int additionalFlowers)
    {
        this.flowersCount = additionalFlowers;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    @Override
    public double getPrice() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getPrice() + additionalPrice ;
    }

    @Override
    public String getName() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getName() + toName;
    }

    @Override
    public int getFlowersCount() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getFlowersCount() + flowersCount ;
    }

    public String getDeliveryType() {
        return deliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        this.deliveryType = deliveryType;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPackType() {
        return packType;
    }

    public void setPackType(String packType) {
        this.packType = packType;
    }
}