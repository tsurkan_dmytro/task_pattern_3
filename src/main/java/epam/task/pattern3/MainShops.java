package epam.task.pattern3;

import epam.task.pattern3.storefcatory.AbstractDelivery;
import epam.task.pattern3.storefcatory.GetBouquetFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainShops {
    public static void main(String[] args){

        GetBouquetFactory bouquetFactory = new GetBouquetFactory();
        String keyMenu = null;
    do {

        System.out.print("Enter the number of pizza template: 1 - 4");
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            keyMenu = br.readLine().toUpperCase();
            System.out.println("Enter the number of pizza units : ");
            int units = Integer.parseInt(br.readLine());
            System.out.println("Bouqueti : " + keyMenu + " units: " + units);

        }catch (IOException e){
            System.out.println("menu problem");
        }

        AbstractDelivery p = bouquetFactory.getDelivery(keyMenu);
        p.create();

    }while (!keyMenu.equals("Q"));
    }
}
