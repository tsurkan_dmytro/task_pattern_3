package epam.task.pattern3.discountvar;

import epam.task.pattern3.BouquetDecorator;

public class SuperDiscont extends BouquetDecorator {
    private final double COST = -20;
    private final String DISCOUNT = " :>Super";

    public SuperDiscont() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setDiscount(DISCOUNT);
    }
}