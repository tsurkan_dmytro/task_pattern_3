package epam.task.pattern3.discountvar;

import epam.task.pattern3.BouquetDecorator;

public class WeekendDiscont extends BouquetDecorator {
    private final double COST = -8;
    private final String DISCOUNT = " :>Weekend";

    public WeekendDiscont() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setDiscount(DISCOUNT);
    }
}