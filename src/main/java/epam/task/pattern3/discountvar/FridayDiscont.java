package epam.task.pattern3.discountvar;

import epam.task.pattern3.BouquetDecorator;

public class FridayDiscont extends BouquetDecorator {
    private final double COST = -5;
    private final String DISCOUNT = "  :>Friday";

    public FridayDiscont() {
        create();
    }

    @Override
    public void create() {
        setAdditionalPrice(COST);
        setDiscount(DISCOUNT);
    }
}
